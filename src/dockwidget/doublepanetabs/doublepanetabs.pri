HEADERS *= \
    $$PWD/doublepanetabs.hpp \
    $$PWD/tabwidget.hpp

SOURCES *= \
    $$PWD/doublepanetabs.cpp \
    $$PWD/dpane_management.cpp \
    $$PWD/tabwidget.cpp
