

#include "doublepanetabs.hpp"


namespace wgt {
void DoublePaneTabs::saveFile() {
    DPN_LOG << Q_FUNC_INFO;
    getCurrentWorkspace()->saveFile();
}

void DoublePaneTabs::openFile(QString path) {
    LOG << "Opening file at" << path;
    bool                    newWorkspaceRequired = true;
    std::vector<Workspace*> leftWorkspaces       = getLeftWorkspaces();
    newWorkspaceRequired &= !spt::apply_first_if_count(
        leftWorkspaces.begin(),
        leftWorkspaces.end(),
        [&](uint index, Workspace* workspace) {
            selectedPane = leftTabs;
            leftTabs->setCurrentIndex(index);
            workspace->openFile(path);
            INFO << "Left workspace can open";
        },
        [&](Workspace* workspace) {
            return workspace->alreadyOpened(path) || workspace->isEmpty();
        });


    std::vector<Workspace*> rightWorkspaces = getRightWorkspaces();
    newWorkspaceRequired &= !spt::apply_first_if_count(
        rightWorkspaces.begin(),
        rightWorkspaces.end(),
        [&](uint index, Workspace* workspace) {
            selectedPane = leftTabs;
            leftTabs->setCurrentIndex(index);
            workspace->openFile(path);
            INFO << "Right workspace can open";
        },
        [&](Workspace* workspace) {
            return workspace->alreadyOpened(path) || workspace->isEmpty();
        });

    if (newWorkspaceRequired) {
        Workspace* newWorkspace = new Workspace(selectedPane);
        newWorkspace->openFile(path);
        selectedPane->addTab(newWorkspace, newWorkspace->getName());
        INFO << "Created new tab for file";
    }

    updateUI();
}

void DoublePaneTabs::newFile() {
    Workspace* emptyWorkspace = new Workspace(this);
    emptyWorkspace->openNewFileMenu();
    selectedPane->addTab(emptyWorkspace, "New file");
}

QString DoublePaneTabs::getWidgetName() const {
    return "DoublePaneTabs";
}


Workspace* DoublePaneTabs::getCurrentWorkspace() const {
    DPN_FUNC_BEGIN
    QWidget* currentTab = selectedPane->widget(
        selectedPane->currentIndex());
    Workspace* currentWorkspace = dynamic_cast<Workspace*>(currentTab);
    DEBUG_ASSERT_TRUE(currentWorkspace != nullptr)
    DPN_FUNC_END
    return currentWorkspace;
}


std::vector<Workspace*> DoublePaneTabs::getLeftWorkspaces() const {
    std::vector<Workspace*> result;
    for (int i = 0; i < leftTabs->count(); ++i) {
        Workspace* tab = dynamic_cast<Workspace*>(leftTabs->widget(i));
        DEBUG_ASSERT_TRUE(tab != nullptr)
        result.push_back(tab);
    }
    return result;
}

std::vector<Workspace*> DoublePaneTabs::getRightWorkspaces() const {
    std::vector<Workspace*> result;
    for (int i = 0; i < rightTabs->count(); ++i) {
        Workspace* tab = dynamic_cast<Workspace*>(rightTabs->widget(i));
        DEBUG_ASSERT_TRUE(tab != nullptr)
        result.push_back(tab);
    }

    return result;
}

Workspace* DoublePaneTabs::getWorkspace(TabWidget* pane, int index) {
    Workspace* tab = dynamic_cast<Workspace*>(pane->widget(index));
    DEBUG_ASSERT_TRUE(tab != nullptr)
    return tab;
}
} // namespace wgt
