

#include "tabwidget.hpp"

TabWidget::TabWidget(QWidget* parent)
    : QTabWidget(parent)
    , eventFilter(this)
    , newTab_pbtn(new QPushButton(this)) {
    newTab_pbtn->resize(15, 15);
    newTab_pbtn->setText("+");
    connect(newTab_pbtn, &QPushButton::pressed, [&]() {
        addTab(new QWidget(this), "");
        setCurrentIndex(count() - 1);
        emit newEmptyTabCreated(count() - 1);
    });
    setCornerWidget(newTab_pbtn);
    initShortcuts();
}

Workspace* TabWidget::getCurrentWorkspace() const {
    return dynamic_cast<Workspace*>(widget(currentIndex()));
}

Workspace* TabWidget::getWorkspace(int tabIndex) const {
    return dynamic_cast<Workspace*>(widget(tabIndex));
}

void TabWidget::resetWidgetAt(QWidget* newWidget, int index) {
    removeTab(index);
    insertTab(index, newWidget, "");
}


TabWidgetEventFilter::TabWidgetEventFilter(TabWidget* _parent) {
    parent = _parent;
    parent->tabBar()->installEventFilter(this);
}

bool TabWidgetEventFilter::eventFilter(QObject* watched, QEvent* event) {
    if (event->type() == QEvent::MouseButtonPress) {
        QMouseEvent* mouseEvent = static_cast<QMouseEvent*>(event);
        if (mouseEvent->button() == Qt::LeftButton) {
            parent->tabBarLeftMouseClicked(
                parent->tabBar()->tabAt(mouseEvent->pos()));
        }

        if (mouseEvent->button() == Qt::MiddleButton) {
            parent->tabBarMiddleMouseClicked(
                parent->tabBar()->tabAt(mouseEvent->pos()));
        }
    } else if (event->type() == QEvent::MouseButtonDblClick) {
        QMouseEvent* mouseEvent = static_cast<QMouseEvent*>(event);
        if (mouseEvent->button() == Qt::LeftButton) {
            parent->tabBarLeftMouseDblClicked(
                parent->tabBar()->tabAt(mouseEvent->pos()));
        }
    }
    return QObject::eventFilter(watched, event);
}

void TabWidget::keyPressEvent(QKeyEvent* event) {
    wgt::KeyEvent keyEvent(event);
    processKeyEvent(keyEvent);
    QWidget::keyPressEvent(event);

    DEBUG_FINALIZE_WINDOW
}

void TabWidget::initShortcuts() {
    shortcuts.addBinding("ctrl+t", "open-new-tab");
    keyboardActions["open-new-tab"] =
        [&](wgt::KBDParameters& internalParameters,
            json&               userParameters) -> bool {
        Q_UNUSED(userParameters)
        Q_UNUSED(internalParameters)

        openNewTab();
        return true;
    };

    shortcuts.addBinding("ctrl+w", "close-current-tab");
    keyboardActions["close-current-tab"] =
        [&](wgt::KBDParameters& internalParameters,
            json&               userParameters) -> bool {
        Q_UNUSED(userParameters)
        Q_UNUSED(internalParameters)

        closeCurrentTab();
        return true;
    };

    shortcuts.addBinding("ctrl+tab", "switch-to-next-tab");
    keyboardActions["switch-to-next-tab"] =
        [&](wgt::KBDParameters& internalParameters,
            json&               userParameters) -> bool {
        Q_UNUSED(userParameters)
        Q_UNUSED(internalParameters)

        setCurrentIndex(
            currentIndex() == (count() - 1) ? 0 : currentIndex() + 1);
        return true;
    };

    shortcuts.addBinding("ctrl+shift+backtab", "switch-to-previous-tab");
    keyboardActions["switch-to-previous-tab"] =
        [&](wgt::KBDParameters& internalParameters,
            json&               userParameters) -> bool {
        Q_UNUSED(userParameters)
        Q_UNUSED(internalParameters)

        setCurrentIndex(
            currentIndex() == 0 ? count() - 1 : currentIndex() - 1);
        return true;
    };
}

bool TabWidget::processKeyEvent(wgt::KeyEvent& event) {
    QString            command = shortcuts.getCommand(event.getSequence());
    wgt::KBDParameters parameters;
    parameters.target = this;
    KBD_INFO << "Tabwidget";
    KBD_LOG << "Command :" << command;
    KBD_LOG << "Shortcut:" << event.getSequence().toString();
    return executeCommand(command, parameters);
}


void TabWidget::openNewTab() {
    this->addTab(new QWidget, "New tab");
    emit newEmptyTabCreated(count() - 1);
}

void TabWidget::closeCurrentTab() {
    removeTab(currentIndex());
}
