#ifndef TABWIDGET_HPP
#define TABWIDGET_HPP

#include <QAction>
#include <QEvent>
#include <QMouseEvent>
#include <QPushButton>
#include <QTabBar>
#include <QTabWidget>
#include <QWidget>

#include "../include/dockwidget/workspace.hpp"
#include <hdebugmacro/all.hpp>
#include <qwgtlib/base/keyboardsupport.hpp>

#define TABWIDGET_DEBUG

#ifdef TABWIDGET_DEBUG
#    define TAB_LOG DebugLogger::log()
#else
#    define TAB_LOG VOID_LOG
#endif

class TabWidget;

class TabWidgetEventFilter : public QObject

{
  public:
    TabWidgetEventFilter(TabWidget* _parent);
    TabWidget* parent;

    // QObject interface
  public:
    bool eventFilter(QObject* watched, QEvent* event) override;
};


class TabWidget
    : public QTabWidget
    , public wgt::KeyboardSupport
{
    Q_OBJECT
  public:
    explicit TabWidget(QWidget* parent = nullptr);
    Workspace* getCurrentWorkspace() const;
    Workspace* getWorkspace(int tabIndex) const;
    void       resetWidgetAt(QWidget* newWidget, int index);
    void       openNewTab();
    void       closeCurrentTab();

  private:
    TabWidgetEventFilter eventFilter;
    QPushButton*         newTab_pbtn;

  signals:
    void tabBarMiddleMouseClicked(int index);
    void tabBarLeftMouseClicked(int index);
    void tabBarLeftMouseDblClicked(int index);
    void newEmptyTabCreated(int index);

    // QWidget interface
  protected:
    void keyPressEvent(QKeyEvent* event) override;


    // KeyboardSupport interface
  public:
    bool processKeyEvent(wgt::KeyEvent& event) override;

  protected:
    void initShortcuts() override;
};


#endif // TABWIDGET_HPP
