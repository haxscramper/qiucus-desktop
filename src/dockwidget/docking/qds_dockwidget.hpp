#ifndef DOCKWIDGET_HPP
#define DOCKWIDGET_HPP


#include <qdocksystem/dockwidget.hpp>

namespace qds {
class DockManager;
}

namespace qds {
class DockWidget : public ads::DockWidget
{
  public:
    DockWidget(DockManager* manager);
};
} // namespace qds

#endif // DOCKWIDGET_HPP
