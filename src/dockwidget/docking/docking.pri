HEADERS *= \
    $$PWD/qds_tab.hpp \
    $$PWD/qds_dockwidget.hpp \
    $$PWD/qds_dockmanager.hpp

SOURCES *= \
    $$PWD/qds_dockmanager.cpp \
    $$PWD/qds_dockwidget.cpp \
    $$PWD/qds_tab.cpp
