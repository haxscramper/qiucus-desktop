

#include "qds_tab.hpp"

namespace qds {
Tab::Tab(std::unique_ptr<QWidget> _widget, QString _title)
    : ads::Tab(std::move(_widget), _title) {
}

Tab::Tab(std::unique_ptr<QWidget> _widget, std::unique_ptr<QWidget> _title)
    : ads::Tab(std::move(_widget), std::move(_title))
//  ,
{
}

}  // namespace qds
