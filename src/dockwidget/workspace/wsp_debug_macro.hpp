#ifndef WSP_DEBUG_MACRO_HPP
#define WSP_DEBUG_MACRO_HPP

#include <hdebugmacro/all.hpp>

//#define WSPWIDGET_DEBUG

#ifdef WSPWIDGET_DEBUG
#    define WSP_LOG LOG
#    define WSP_FUNC_BEGIN DEBUG_FUNCTION_BEGIN
#    define WSP_FUNC_END DEBUG_FUNCTION_END
#    define WSP_FUNC_RET(message) DEBUG_FUNCTION_PRE_RETURN(message)
#else
#    define WSP_LOG VOID_LOG
#    define WSP_FUNC_BEGIN
#    define WSP_FUNC_END
#    define WSP_FUNC_RET(message)
#endif


#endif // WSP_DEBUG_MACRO_HPP
