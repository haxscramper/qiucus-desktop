#ifndef WORKSPACE_H
#define WORKSPACE_H

#include <QDebug>
#include <QGridLayout>
#include <QMessageBox>
#include <QWidget>

#include <memory>

#include <datawidget/linkviewer.hpp>
#include <datawidget/noteviewer.hpp>
#include <qwgtlib/base/widgetinterface.hpp>
#include <qwgtlib/base/widgetmanageable.hpp>
#include <qwgtlib/pdfviewer.hpp>
#include <qwgtlib/textedit/codeeditor.hpp>
#include <qwgtlib/textedit/markdowneditor.hpp>
#include <qwgtlib/textedit/richtexteditor.hpp>
#include <qwgtlib/webview.hpp>

#include "wsp_debug_macro.hpp"
#include "wsp_newtabmenu.hpp"


class Workspace
    : public QWidget
    , public WidgetInterface
{
    Q_OBJECT
  public:
    explicit Workspace(QWidget* parent = nullptr);
    ~Workspace() override;

    bool canOpen(QString path);
    bool isSupported(QString extension);
    bool alreadyOpened(QString path);
    bool isEmpty();

    QString getFile();
    QString getName();

    void setIconsDir(const QString& value);

  private:
    QGridLayout*    mainLayout;
    QWidget*        containedWidget = nullptr;
    WSP_NewTabMenu* newTabMenu      = nullptr;
    QString         newFileMenuLayout;
    QString         iconsDir;

    void initUI() override;
    void updateUI() override;
    void setToNewFileMode();
    void setToOpenedFileMode();

    std::unique_ptr<wgtm::WidgetManageable> widgetHandle;
    wgtm::WidgetManageable* resolveWidget(QString extension);

    enum class WorkspaceState
    {
        NewTabMenu,
        FileOpened
    };

    WorkspaceState workspaceState;

  signals:
    void pathChanged(QString newPath);
    void nameChanged(QString newName);

  public slots:
    void saveFile();
    void newFile(QString extension);
    void openNewFileMenu();
    void saveFileAs();
    void openFile(QString path);
};

#endif // WORKSPACE_H
