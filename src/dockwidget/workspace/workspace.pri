HEADERS *= \
    $$PWD/workspace.hpp \
    $$PWD/wsp_debug_macro.hpp \
    $$PWD/wsp_newtabbutton.hpp \
    $$PWD/wsp_newtabmenu.hpp

SOURCES *= \
    $$PWD/workspace.cpp \
    $$PWD/workspace_file.cpp \
    $$PWD/workspace_ui.cpp \
    $$PWD/wsp_newtabbutton.cpp \
    $$PWD/wsp_newtabmenu.cpp
