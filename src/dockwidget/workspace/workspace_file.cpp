

#include "workspace.hpp"


void Workspace::saveFileAs() {
    widgetHandle->saveFileAs();
}

// TODO Check for empty pointer and add documentation
void Workspace::openFile(QString path) {
    WSP_FUNC_BEGIN

    if (workspaceState == WorkspaceState::NewTabMenu) {
        widgetHandle.reset(resolveWidget(QFileInfo(path).suffix()));
        setToOpenedFileMode();
        WSP_LOG << "Setting to open file mode";
    }

    if (this->canOpen(path)) {
        WSP_LOG << "Current widget can open file";
        widgetHandle->openFile(path);
    } else {
        WSP_LOG << "Current widget cannot open file";
        WSP_LOG << "Resetting widget";
        widgetHandle.reset(resolveWidget(QFileInfo(path).suffix()));
        updateUI();
        widgetHandle->openFile(path);
    }

    WSP_FUNC_END
}


// TODO Check for empty pointer and add documentation
bool Workspace::isSupported(QString extension) {
    return widgetHandle->isSupported(extension);
}

//! \brief Check if file at path is already opened by this workspace
//! \return True if opened otherwise false
bool Workspace::alreadyOpened(QString path) {
    if (widgetHandle.get() == nullptr) {
        return false;
    } else {
        return (widgetHandle->getFile() == path);
    }
}


bool Workspace::isEmpty() {
    return workspaceState == WorkspaceState::NewTabMenu;
}


void Workspace::newFile(QString extension) {
    WSP_FUNC_BEGIN

    if (workspaceState == WorkspaceState::NewTabMenu) {
        workspaceState = WorkspaceState::FileOpened;
        widgetHandle.reset(resolveWidget(extension));
        setToOpenedFileMode();
        widgetHandle->newFile();
    } else if (workspaceState == WorkspaceState::FileOpened) {
        widgetHandle->saveFile();
        widgetHandle->newFile();
    }

    WSP_FUNC_END
}

//! \brief Check if workspace can possibly open this file
//! \return True current widget can open or if workspace in new file menu
//! mode and false if no widget is present
bool Workspace::canOpen(QString path) {
    WSP_FUNC_BEGIN
    if (workspaceState == WorkspaceState::NewTabMenu) {
        WSP_FUNC_RET("Switching from new tab menu")
        return true;
    } else {
        WSP_FUNC_RET("Already has file opened")
        return (
            widgetHandle->isSupported(QFileInfo(path).suffix())
            || widgetHandle->isAllowed(QFileInfo(path).suffix()));
    }
}


// TODO Check for empty pointer and add documentation
void Workspace::saveFile() {
    if (workspaceState == WorkspaceState::FileOpened) {
        widgetHandle->saveFile();
    }
}


/*! \brief Return path of the current file if any
 *  \return Absolute path or empty string if not file opened
 */
QString Workspace::getFile() {
    if (widgetHandle.get() == nullptr) {
        return "";
    } else {
        return widgetHandle->getFile();
    }
}


/*! \brief Return name of the current file if any
 *  \return basename + extension of currently opened
 *  file or empty string if no file has been opened
 */
QString Workspace::getName() {
    if (widgetHandle.get() == nullptr) {
        return "untitled";
    } else {
        QString name = widgetHandle->getFileName();
        if (name == "." || name.isEmpty()) {
            return "untitled";
        } else {
            return widgetHandle->getFileName();
        }
    }
}
