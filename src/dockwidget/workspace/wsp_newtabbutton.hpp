#ifndef WSP_NEWFILEBUTTON_HPP
#define WSP_NEWFILEBUTTON_HPP

#include <QFont>
#include <QFontMetricsF>
#include <QGraphicsItem>
#include <QPainter>
#include <QPen>
#include <QRectF>
#include <hsupport/qjson.hpp>

class WSP_NewTabMenu;

class WSP_NewTabButton : public QGraphicsItem
{
  public:
    WSP_NewTabButton(WSP_NewTabMenu* _menu);
    QString getButtonName() const;
    void    setButtonName(const QString& value);
    uint    getRow() const;
    void    setRow(const uint& value);
    void    updatePos();
    uint    getColumn() const;
    void    setColumn(const uint& value);
    void    fromJson(json& settings);

  private:
    QRectF  buttonRect;
    bool    isHovered = false;
    QString buttonName;
    QString extension;
    QRectF  textSize;
    uint    row;
    uint    column;
    QPointF pos;

    WSP_NewTabMenu* menu;

    // QGraphicsItem interface
  public:
    QRectF boundingRect() const override;
    void   paint(
          QPainter*                       painter,
          const QStyleOptionGraphicsItem* option,
          QWidget*                        widget) override;
    void hoverEnterEvent(QGraphicsSceneHoverEvent* event) override;
    void hoverLeaveEvent(QGraphicsSceneHoverEvent* event) override;
    void mousePressEvent(QGraphicsSceneMouseEvent* event) override;
};


#endif // WSP_NEWFILEBUTTON_HPP
