#include "qiucus_application.hpp"

#include <QDir>
#include <hdebugmacro/all.hpp>
#include <hsupport/misc.hpp>

QString QiucusApplication::getLastScratchFile() {
    return lastScratchFile;
}


void QiucusApplication::setLastScratchFile(const QString& value) {
    lastScratchFile = value;
}


QString QiucusApplication::getSettingsFolder() {
    return settingsFolder;
}


QString QiucusApplication::getLastFolder() {
    return lastFolder;
}


void QiucusApplication::setLastFolder(QString folder) {
    lastFolder = folder;
}


QString QiucusApplication::getScratchFolder() {
    return settingsFolder;
}

void QiucusApplication::setSettingsFolder(QString folder) {
    settingsFolder = folder;
}

QString QiucusApplication::newScratchFileFolder() {
    QString file = getScratchFolder() + spt::getNewScratchFile();
    while (QFile(file).exists()) {
        file = getScratchFolder() + spt::getNewScratchFile();
    }
    setLastScratchFile(file);
    return file;
}
