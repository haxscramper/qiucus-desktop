#! /usr/bin/perl

use strict;
use warnings;
use JSON;
use Cwd qw(realpath getcwd);
use File::Spec;

my %settings = (
    "settings_folder" => realpath(getcwd . "/../../settings"),
    "tmp_folder" => realpath(getcwd . "../../tmp")
);

open(my $file, '>', "initial_settings.json");
my $json = to_json(\%settings, {pretty => 1});
print $file $json;
close $file;
