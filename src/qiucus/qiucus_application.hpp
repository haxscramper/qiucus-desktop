#ifndef QIUCUSAPPLICATION_HPP
#define QIUCUSAPPLICATION_HPP

#include "mainwindow.hpp"
#include <QApplication>
#include <QTime>
#include <hsupport/qjson.hpp>

class QiucusApplication : public QApplication
{
  public:
    QiucusApplication(int& argc, char** argv);
    ~QiucusApplication();

    QString getLastScratchFile();
    void    setLastScratchFile(const QString& value);
    QString getSettingsFolder();
    QString getScratchFolder();
    void    setSettingsFolder(QString folder);
    QString newScratchFileFolder();
    QString getLastFolder();
    void    setLastFolder(QString folder);
    QString qiucusSourceDir();

    void readAppSettings(
        QString path     = "init_settings.json",
        bool    relative = true);
    void saveAppSettings(
        QString path     = "init_settings.json",
        bool    relative = true);

    void  saveApplicationState(QString name = "session_state.json");
    void  readApplicationState(QString name = "session_state.json");
    void  appyApplicationState();
    void  show();
    json& getSettings();

  private:
    QString lastScratchFile;
    QString lastFolder;
    QString settingsFolder;
    /// Application settings - might not reflect acual state of the
    /// application.
    json       settings;
    MainWindow window;
};

#endif // QIUCUSAPPLICATION_HPP
