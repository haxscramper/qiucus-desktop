#ifndef TABSELECTOR_HPP
#define TABSELECTOR_HPP

#include <QToolBar>

namespace wgt {
class BarTab : QWidget
{
    BarTab();
    Q_OBJECT
};

/*!
 * \brief The TabSelector class shows all tabs opened in mainwindo's dock
 * manager in one place
 */
class TabSelector : public QToolBar
{
    Q_OBJECT
  public:
    explicit TabSelector(QWidget* parent = nullptr);
};
}  // namespace wgt

#endif  // TABSELECTOR_HPP
