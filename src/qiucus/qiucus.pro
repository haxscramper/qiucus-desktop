TEMPLATE = app
TARGET = qiucus

QT *= xml core widgets webenginewidgets
QMAKE_CXXFLAGS *= -std=c++17 -w

TOP_DIR=$$PWD/../../../

include ($$TOP_DIR/qwgtlib/include.pri) # Widgets
include ($$TOP_DIR/qdocksystem/include.pri) # Docking system
include ($$TOP_DIR/qdelib/include.pri) # Dataentity

MISC_DIR=$$TOP_DIR/haxscamper-misc/cpp

include ($$MISC_DIR/hsupport/include.pri)
include ($$MISC_DIR/hdebugmacro/include.pri)
include ($$MISC_DIR/halgorithm/include.pri)

include ($$PWD/../datawidget/include.pri) # Datawidgets
include ($$PWD/../dockwidget/include.pri) # Dockwidget

LIBS += -L$$/usr/lib64 -lqscintilla2_qt5 # QScintilla

unix: PKGCONFIG += poppler-qt5
unix: CONFIG += link_pkgconfig

SOURCES *= $$PWD/*.cpp
HEADERS *= $$PWD/*.hpp
