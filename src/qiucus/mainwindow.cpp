
/*!
 * \file mainwindow.cpp
 */

//===    Qt    ===//
#include <QSplitter>


//=== Internal ===//
#include <dockwidget/doublepanetabs.hpp>
#include <qwgtlib/filetreeviewer.hpp>


//=== Sibling  ===//
#include "mainwindow.hpp"


//  ////////////////////////////////////


MainWindow::MainWindow(QWidget* parent)
    : QMainWindow(parent)
    , mainTabs(new wgt::DoublePaneTabs(this))
    , fileTree(new wgt::FileTreeViewer(this))
    , splitter(new QSplitter(this))
//  ,
{
    setCentralWidget(splitter);
    splitter->addWidget(fileTree);
    splitter->addWidget(mainTabs);
    splitter->setChildrenCollapsible(false);
    splitter->setStretchFactor(1, 7);
    resize(1500, 900);
    initSignals();
}

void MainWindow::setFileTreeFolder(QString dir) {
    fileTree->setRoot(dir);
}


QString MainWindow::getFileTreeFolder() const {
    return fileTree->getRoot();
}

void MainWindow::setSettings(json settings) {
    if (settings.find("filetree_root") != settings.end()) {
        fileTree->setRoot(
            QString::fromStdString(settings["filetree_root"]));
    }
}

json MainWindow::getSettings() {
    json settings;

    settings["filetree_root"] = fileTree->getRoot().toStdString();

    return settings;
}

void MainWindow::initSignals() {
    connect(
        fileTree,
        &wgt::FileTreeViewer::fileSelected,
        mainTabs,
        &wgt::DoublePaneTabs::openFile);
}
