#pragma once

#include <qwgtlib/base/widgetmanageable.hpp>

#include <QDebug>
#include <QFile>
#include <QFileInfo>
#include <QGridLayout>
#include <QWidget>

#include "dwgt_debug_macro.hpp"


#define CALLED_ONCE                                                       \
    static int _counter = 0;                                              \
    assert(_counter < 1);                                                 \
    _counter++;


/// DataEntity-related widgets
namespace dwgt {
class DataWidget : public wgtm::WidgetManageable
{
  public:
    explicit DataWidget() = default;
    virtual bool isOwner() const;
};
} // namespace dwgt
