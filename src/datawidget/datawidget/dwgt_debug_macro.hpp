#ifndef DWGT_DEBUG_MACRO_HPP
#define DWGT_DEBUG_MACRO_HPP

#include <hdebugmacro/all.hpp>

//#define DWGTWIDGET_DEBUG

#ifdef DWGTWIDGET_DEBUG
#    define DWGT_LOG LOG
#    define DWGT_FUNC_BEGIN DEBUG_FUNCTION_BEGIN
#    define DWGT_FUNC_END DEBUG_FUNCTION_END
#    define DWGT_FUNC_RET(message) DEBUG_FUNCTION_PRE_RETURN(message)
#else
#    define DWGT_LOG VOID_LOG
#    define DWGT_FUNC_BEGIN
#    define DWGT_FUNC_END
#    define DWGT_FUNC_RET(message)
#endif

#endif // DWGT_DEBUG_MACRO_HPP
