HEADERS += \
    $$PWD/qscalendarviewer.hpp \
    $$PWD/calendarmodel.hpp \
    $$PWD/eventgraphicsitem.hpp \
    $$PWD/calendarmodelviewer.hpp

SOURCES += \
    $$PWD/qscalendarviewer.cpp \
    $$PWD/calendarmodel.cpp \
    $$PWD/eventgraphicsitem.cpp \
    $$PWD/calendarmodelviewer.cpp
