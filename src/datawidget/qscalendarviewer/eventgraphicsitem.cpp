#include "eventgraphicsitem.hpp"
#include <support_macro.hpp>

EventGraphicsItem::EventGraphicsItem() {
}

QRectF EventGraphicsItem::boundingRect() const {
    return QRectF(0, 0, 20, 20);
}

void EventGraphicsItem::paint(
    QPainter*                       painter,
    const QStyleOptionGraphicsItem* option,
    QWidget*                        widget) {
    TEMPORARY_UNUSED(painter)
    TEMPORARY_UNUSED(option)
    TEMPORARY_UNUSED(widget)
}
