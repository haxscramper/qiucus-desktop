#ifndef LISTVIEWER_H
#define LISTVIEWER_H

#include <QHBoxLayout>
#include <QPushButton>

#include <dataentity.hpp>

#include "listmodel.hpp"
#include "matrixviewer.hpp"
#include "tablemodel.hpp"
#include "vectorviewer.hpp"
#include <datawidget.hpp>
#include <qstable/qstable.hpp>

namespace dwgt {
class TableViewer : public DataWidget
{
  public:
    TableViewer();

  private:
    MatrixViewer*  matrixViewer = nullptr;
    VectorViewer*  vectorViewer = nullptr;
    QHBoxLayout*   mainLayout   = nullptr;
    TableModel*    tableModel   = nullptr;
    ListModel*     listModel    = nullptr;
    dent::QSTable* list;
    QPushButton*   savePBtn;

    // WidgetManageable interface
  public slots:
    void writeFile(QString path);
    // FIX hides function
};
} // namespace dwgt

#endif // LISTVIEWER_H
