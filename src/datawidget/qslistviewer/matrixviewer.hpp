#ifndef MATRIXVIEWER_H
#define MATRIXVIEWER_H

#include <QTableView>
#include <QWidget>


class MatrixViewer : public QTableView
{
    Q_OBJECT
  public:
    explicit MatrixViewer(QWidget* parent = nullptr);
};

#endif // MATRIXVIEWER_H
