#include "listmodel.hpp"

ListModel::ListModel(QObject* parent) : QAbstractListModel(parent) {
    array = new dent::NDimArray;
    array->reshape(Slice().setX(40));
    for (int i = 0; i < 40; i++) {
        array->at(i)->setValue(QString("Hello"));
    }
}

int ListModel::rowCount(const QModelIndex& parent) const {
    Q_UNUSED(parent)
    return array->shape().sizeX();
}

QVariant ListModel::data(const QModelIndex& index, int role) const {
    assert(index.isValid());

    if (role == Qt::DisplayRole) {
        return (array->at(index.row(), this->column, this->sheet))
            ->toVariant();
    }

    return QVariant();
}

QVariant* ListModel::at(const QModelIndex& index) const {
    TEMPORARY_UNUSED(index)
    return new QVariant();
    // FIXME Does this funciton even needed
}

bool ListModel::canFetchMore(const QModelIndex& parent) const {
    TEMPORARY_UNUSED(parent)
    return false;
}

void ListModel::fetchMore(const QModelIndex& parent) {
    TEMPORARY_UNUSED(parent)
}
