#ifndef LISTMODEL_H
#define LISTMODEL_H

#include <ndimarray.hpp>
#include <slice.hpp>

#include <QAbstractListModel>
#include <QDebug>

class ListModel : public QAbstractListModel
{
    Q_OBJECT
  public:
    explicit ListModel(QObject* parent = nullptr);
    int      rowCount(const QModelIndex& parent) const override;
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole)
        const override;
    QVariant* at(const QModelIndex& index) const;

  protected:
    bool canFetchMore(const QModelIndex& parent) const override;
    void fetchMore(const QModelIndex& parent) override;

  private:
    int              column = 0;
    int              sheet  = 0;
    dent::NDimArray* array;
};

#endif // LISTMODEL_H
