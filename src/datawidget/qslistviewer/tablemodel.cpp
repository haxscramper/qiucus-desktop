#include "tablemodel.hpp"

TableModel::TableModel() {
    array = new dent::NDimArray;
    array->reshape(Slice().setX(40).setY(40));
}

int TableModel::rowCount(const QModelIndex& /*parent*/) const {
    return array->shape().sizeX();
}

int TableModel::columnCount(const QModelIndex& /*parent*/) const {
    return array->shape().sizeY();
}

QVariant TableModel::data(const QModelIndex& index, int role) const {
    if (!index.isValid()) {
        return QVariant();
    }

    if (role == Qt::DisplayRole) {
        return array->at(index.row(), index.column(), this->sheet)
            ->toVariant();
    } else {
        return QVariant();
    }
}
