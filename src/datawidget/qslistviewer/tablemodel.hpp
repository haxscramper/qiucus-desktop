#ifndef ARRAYMODEL_H
#define ARRAYMODEL_H

#include <QAbstractTableModel>
#include <ndimarray.hpp>
class TableModel : public QAbstractTableModel
{
    Q_OBJECT
  public:
    TableModel();
    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    int columnCount(
        const QModelIndex& parent = QModelIndex()) const override;
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole)
        const override;

  private:
    int              sheet = 0;
    dent::NDimArray* array = nullptr;
    dent::NDimView*  view  = nullptr;
};

#endif // ARRAYMODEL_H
