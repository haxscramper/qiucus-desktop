#ifndef VECTORVIEWER_H
#define VECTORVIEWER_H

#include "listmodel.hpp"
#include <QDebug>
#include <QListView>
#include <QObject>
#include <QWidget>


class VectorViewer : public QListView
{
    Q_OBJECT
  public:
    explicit VectorViewer(QWidget* parent = nullptr);

  private:
    ListModel* listModel;

  signals:
    void cellSelected(QVariant* data);

    // QAbstractItemView interface
  public:
    void setModel(QAbstractItemModel* model);
};

#endif // VECTORVIEWER_H
