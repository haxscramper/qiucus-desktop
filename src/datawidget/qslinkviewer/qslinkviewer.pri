HEADERS *= \
    $$PWD/linkviewer.hpp \
    $$PWD/linkviewer_widget.hpp

SOURCES *= \
    $$PWD/linkviewer.cpp \
    $$PWD/linkviewer_management.cpp \
    $$PWD/linkviewer_ui.cpp \
    $$PWD/linkviewer_widget.cpp
