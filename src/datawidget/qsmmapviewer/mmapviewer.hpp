#ifndef MMAPVIEWER_H
#define MMAPVIEWER_H

#include "../datawidget.hpp"
#include "mindmapview.hpp"
#include <QDebug>
#include <QGraphicsRectItem>
#include <graphview/graphviewer.h>
#include <memory>
#include <qsmmap/qsmmap.hpp>

namespace dwgt {
class MMapViewer
    : public DataWidget
    , public GraphViewer
{
  public:
    MMapViewer();
    ~MMapViewer();

  private:
    dent::QSMmap*                map;
    std::unique_ptr<MindMapView> mapView;
    QGridLayout*                 mainLayout;
    void                         setupScene();

    // DataWidget interface
  protected:
    void setupUI() override;
    void setupContent() override;

    // WidgetManageable interface
  public slots:
    void openFile(QString path) override;
    void debugName() override;

  protected:
    void writeFile(QFile& file) override;
    void readFile(QFile& file) override;

    // QWidget interface
  protected:
    void mousePressEvent(QMouseEvent* event) override;
};
} // namespace dwgt

#endif // MMAPVIEWER_H
