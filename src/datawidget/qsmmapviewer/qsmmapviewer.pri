HEADERS += \
    $$PWD/mmapviewer.hpp \
    $$PWD/mindedgeview.h \
    $$PWD/mindmapview.hpp

SOURCES += \
    $$PWD/mmapviewer.cpp \
    $$PWD/mindedgeview.cpp \
    $$PWD/mindmapview.cpp
