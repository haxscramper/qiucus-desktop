#ifndef MINDGRAPH_HPP
#define MINDGRAPH_HPP

#include "QGraphicsItem"
#include <QDebug>
#include <memory>
#include <qsmmap/qsmmap.hpp>

class QSMmap;

class MindMapView
{
  public:
    MindMapView();
    ~MindMapView();
    std::shared_ptr<QGraphicsScene> getScene() const;
    void setScene(const std::shared_ptr<QGraphicsScene>& value);

  private:
    std::shared_ptr<QSMmap>         mindMap;
    std::shared_ptr<QGraphicsScene> scene;
    QList<QGraphicsItem*>           getAllItems();
    //    void reexportScene();
};

#endif // MINDGRAPH_HPP
