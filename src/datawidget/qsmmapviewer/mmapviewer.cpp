#include "mmapviewer.hpp"

namespace dwgt {
void MMapViewer::openFile(QString path) {
    currentFile = path;
    map->readFile(path);
}
void MMapViewer::writeFile(QFile& file) {
    map->writeFile(file);
}
void MMapViewer::debugName() {
    qDebug() << "MMapViewer::debugName";
}

void MMapViewer::readFile(QFile& file) {
}

MMapViewer::MMapViewer()
    : mapView(std::make_unique<MindMapView>())
    , mainLayout(new QGridLayout(this)) {
    qDebug() << "Mind map viewer constructor has been called";
    this->setupUI();
    this->setupScene();
    this->setupContent();
    qDebug() << "MMap viewer constructed";
}

MMapViewer::~MMapViewer() {
    qDebug() << "MMapViewer destroyed";
}

void MMapViewer::setupUI() {
    qDebug() << "MMapViewer::setupUIi has been called";
    mainLayout->addWidget(view.get());
    this->setLayout(mainLayout);
}

void MMapViewer::setupContent() {
    scene->addText("Hello world!");
    QBrush blueBrush(Qt::blue);
    QPen   outlinePen(Qt::black);
    outlinePen.setWidth(2);
    auto rectangle = scene->addRect(
        100, 0, 80, 100, outlinePen, blueBrush);
    rectangle->setFlag(QGraphicsItem::ItemIsMovable);
}

void MMapViewer::setupScene() {
    qDebug() << "MMapViewer::setupScene has been called";
    view.get()->setCacheMode(QGraphicsView::CacheBackground);
    view.get()->setViewportUpdateMode(
        QGraphicsView::BoundingRectViewportUpdate);
    view.get()->setRenderHint(QPainter::Antialiasing);
    view.get()->setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
    view.get()->scale(qreal(1.000001), qreal(1.000001));
    view.get()->setScene(scene.get());
}

void MMapViewer::mousePressEvent(QMouseEvent* event) {
    qDebug() << "MMapViewer::mousePressEvent";
    qDebug() << "Scene containts" << scene->items().count() << "items";
}
} // namespace dwgt
