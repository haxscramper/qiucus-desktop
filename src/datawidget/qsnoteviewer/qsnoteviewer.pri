HEADERS *= \
    $$PWD/noteviewer.hpp \
    $$PWD/noteviewer_widget.hpp

SOURCES *= \
    $$PWD/noteviewer.cpp \
    $$PWD/noteviewer_widget.cpp
