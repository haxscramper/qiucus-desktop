#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>
#include <QSplitter>
#include <qwidget_cptr.hpp>
#include <textedit/codeeditor.hpp>
#include "ui_stylesheetpreview.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT

  public:
    MainWindow(QWidget* parent = 0);
    ~MainWindow();

  private:
    QSplitter*                          splitter;
    spt::qwidget_cptr<wgtm::CodeEditor> codeEditor;
    Ui::StyleSheetPreview* const        styleSheetPreviewForm;
    spt::qwidget_cptr<QWidget>          previewWidget;
};

#endif // MAINWINDOW_HPP
