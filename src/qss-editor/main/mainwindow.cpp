#include "mainwindow.hpp"


MainWindow::MainWindow(QWidget* parent)
    : QMainWindow(parent)
    , codeEditor(new wgtm::CodeEditor(this))
    , styleSheetPreviewForm(new Ui::StyleSheetPreview)
    , previewWidget(new QWidget(this))
//#  ,
{
    styleSheetPreviewForm->setupUi(previewWidget);
    splitter = new QSplitter(this);
    setCentralWidget(splitter);
    splitter->setChildrenCollapsible(false);
    splitter->addWidget(codeEditor);
    splitter->addWidget(previewWidget);

    splitter->setStretchFactor(0, 3);
    splitter->setStretchFactor(1, 1);

    codeEditor->setLexer(spt::LexerType::Qss);

    connect(codeEditor, &wgtm::CodeEditor::buildRequested, [&]() {
        previewWidget->setStyleSheet(codeEditor->getText());
    });
}

MainWindow::~MainWindow() {
}
