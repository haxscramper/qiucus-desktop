include($$PWD/../buildflags.pri)
#include($$QIUCUS_PARENT_DIR/mupdf-qt/mupdf-qt_include.pri)

SOURCES += \
    main.cpp \
    mainwindow.cpp

HEADERS += mainwindow.hpp

# Widgets
  PRE_TARGETDEPS += $$BUILD_DIR/widgets/libwidgets.a
  LIBS += -L$$BUILD_DIR/widgets -lwidgets

# Support
  PRE_TARGETDEPS += $$BUILD_DIR/support/libsupport.a
  LIBS += -L$$BUILD_DIR/support -lsupport

# Baseclasses
  PRE_TARGETDEPS += $$BUILD_DIR/baseclasses/libbaseclasses.a
  LIBS += -L$$BUILD_DIR/baseclasses -lbaseclasses

# QScintilla
  LIBS += -L$$QIUCUS_PARENT_DIR/Qt4Qt5 -lqscintilla2_qt5


unix: PKGCONFIG += poppler-qt5
unix: CONFIG += link_pkgconfig

FORMS += \
    stylesheetpreview.ui
