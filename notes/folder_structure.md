* source - main project source
* wip - temporary work-in-progress tests
* tests - unit test code
* settings - application settings
* tmp - temporary files
* external = external git submodules
* old - old files
* documentation files
* notes - my notes about the project
